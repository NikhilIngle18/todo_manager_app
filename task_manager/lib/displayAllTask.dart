import 'package:flutter/material.dart';
import 'model/modelDisplayAll.dart';

import 'view/displayOverdue.dart';

bool isComplete = true;
Color? checktaskProgressColor(int index) {
  if (alltaskdetailsList[index].taskCompleted) {
    return Colors.green;
  } else if (alltaskdetailsList[index].taskOverdue) {
    return Colors.red;
  } else {
    return alltaskdetailsList[index].taskProgressStatusColor;
  }
}

Widget taskprogress(int index) {
  if (alltaskdetailsList[index].taskOverdue) {
    return const Text(
      "Overdue",
      style: TextStyle(color: Colors.red),
    );
  } else if (alltaskdetailsList[index].taskCompleted) {
    return const Text(
      "100%",
      style: TextStyle(color: Colors.green),
    );
  } else {
    return Text(
      "${alltaskdetailsList[index].taskProgressStatus}",
      style:
          TextStyle(color: alltaskdetailsList[index].taskProgressStatusColor),
    );
  }
}

Color setColor(int index) {
  if (alltaskdetailsList[index].taskCompleted) {
    return Colors.grey;
  } else {
    Colors.green;
  }
  return Colors.transparent;
}

Color setBorderColor(int index) {
  if (alltaskdetailsList[index].taskCompleted) {
    return Colors.grey;
  } else {
    Colors.green;
  }
  return Colors.green;
}

Widget taskIsCompleted(int index) {
  return alltaskdetailsList[index].taskCompleted
      ? Container(
          height: 25,
          width: 25,
          decoration: BoxDecoration(
              border: Border.all(width: 1.5, color: Colors.grey),
              borderRadius: const BorderRadius.all(Radius.circular(8)),
              shape: BoxShape.rectangle),
          child: const Icon(
            Icons.more_horiz_rounded,
            color: Colors.grey,
            size: 18,
          ))
      : Container(
          height: 25,
          width: 25,
          decoration: BoxDecoration(
              border: Border.all(width: 1.5, color: Colors.green),
              borderRadius: const BorderRadius.all(Radius.circular(8)),
              shape: BoxShape.rectangle),
          child: const Icon(
            Icons.more_horiz_rounded,
            color: Colors.green,
            size: 18,
          ));
}

String checktaskProgress1(int index) {
  if (alltaskdetailsList[index].taskCompleted) {
    return "Completed";
  } else if (alltaskdetailsList[index].taskOverdue) {
    return "Overdue";
  } else {
    return "In Progress";
  }
}

Widget checkTaskDetails(int index) {
  if (alltaskdetailsList[index].taskStarted) {
    return Padding(
      padding: const EdgeInsets.only(top: 20.0),
      child: Stack(
        children: [
          SizedBox(
            height: 30,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Stack(children: [
                  Container(
                    height: 2,
                    width: 360,
                    color: Colors.grey,
                  ),
                  Positioned(
                    child: Container(
                      height: 2,
                      width: 150,
                      color: alltaskdetailsList[index].taskProgressStatusColor,
                    ),
                  ),
                ])
              ],
            ),
          ),
          Positioned(
            left: 10,
            bottom: 3,
            child: Row(
              children: [
                Container(
                  width: 25,
                  height: 25,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: alltaskdetailsList[index].taskProgressStatusColor,
                  ),
                  child: isComplete
                      ? const Icon(
                          Icons.check,
                          color: Colors.white,
                          size: 15,
                        )
                      : const Icon(Icons.circle, color: Colors.white),
                ),
              ],
            ),
          ),
          Positioned(
            left: 100,
            bottom: 3,
            child: Row(
              children: [
                Container(
                  width: 25,
                  height: 25,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: alltaskdetailsList[index].taskProgressStatusColor,
                  ),
                  child: isComplete
                      ? const Icon(
                          Icons.check,
                          color: Colors.white,
                          size: 15,
                        )
                      : const Icon(Icons.circle, color: Colors.white),
                ),
              ],
            ),
          ),
          Positioned(
            left: 190,
            bottom: 3,
            child: Row(
              children: [
                Container(
                  width: 25,
                  height: 25,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.white,
                      border: Border.all(
                          color:
                              alltaskdetailsList[index].taskProgressStatusColor,
                          width: 2)),
                  child: !isComplete
                      ? const Icon(Icons.check)
                      : Icon(Icons.circle,
                          color:
                              alltaskdetailsList[index].taskProgressStatusColor,
                          size: 10),
                ),
              ],
            ),
          ),
          Positioned(
            left: 280,
            bottom: 3,
            child: Row(
              children: [
                Container(
                  width: 25,
                  height: 25,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.white,
                      border: Border.all(color: Colors.grey, width: 2)),
                  child: !isComplete
                      ? const Icon(Icons.check)
                      : const Icon(Icons.circle,
                          color: Color.fromARGB(170, 120, 120, 120), size: 10),
                ),
              ],
            ),
          ),
          Positioned(
            left: 360,
            bottom: 3,
            child: Row(
              children: [
                Container(
                  width: 25,
                  height: 25,
                  decoration: const BoxDecoration(
                    shape: BoxShape.circle,
                    color: Color.fromARGB(255, 184, 184, 183),
                  ),
                  child: !isComplete
                      ? const Icon(Icons.check)
                      : const Icon(Icons.circle, color: Colors.white, size: 24),
                ),
              ],
            ),
          )
        ],
      ),
    );
  } else if (alltaskdetailsList[index].taskCompleted) {
    return Padding(
      padding: const EdgeInsets.only(top: 20.0),
      child: Stack(
        children: [
          SizedBox(
            height: 30,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Stack(children: [
                  Container(
                    height: 2,
                    width: 360,
                    color: Colors.grey,
                  ),
                  Positioned(
                    child:
                        Container(height: 2, width: 360, color: Colors.green),
                  ),
                ])
              ],
            ),
          ),
          Positioned(left: 10, bottom: 3, child: completed()),
          Positioned(left: 100, bottom: 3, child: completed()),
          Positioned(left: 190, bottom: 3, child: completed()),
          Positioned(left: 280, bottom: 3, child: completed()),
          Positioned(
            left: 360,
            bottom: 3,
            child: completed(),
          )
        ],
      ),
    );
  } else if (alltaskdetailsList[index].taskOverdue) {
    return Padding(
      padding: const EdgeInsets.only(top: 20.0),
      child: Stack(
        children: [
          SizedBox(
            height: 30,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Stack(children: [
                  Container(
                    height: 2,
                    width: 360,
                    color: Colors.grey,
                  ),
                  Positioned(
                    child: Container(height: 2, width: 360, color: Colors.red),
                  ),
                ])
              ],
            ),
          ),
          Positioned(left: 10, bottom: 3, child: overdue(index)),
          Positioned(left: 100, bottom: 3, child: overdue(index)),
          Positioned(left: 190, bottom: 3, child: overdue(index)),
          Positioned(left: 280, bottom: 3, child: overdue(index)),
          Positioned(
            left: 360,
            bottom: 3,
            child: overdue(index),
          )
        ],
      ),
    );
  } else {
    return Padding(
        padding: const EdgeInsets.only(top: 20.0),
        child: Stack(
          children: [
            SizedBox(
              height: 30,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Stack(children: [
                    Container(
                      height: 2,
                      width: 360,
                      color: Colors.grey,
                    ),
                    Positioned(
                      child: Container(
                        height: 2,
                        width: 0,
                        color:
                            alltaskdetailsList[index].taskProgressStatusColor,
                      ),
                    ),
                  ])
                ],
              ),
            ),
            Positioned(
              left: 10,
              bottom: 3,
              child: Row(
                children: [
                  Container(
                    width: 25,
                    height: 25,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.white,
                        border: Border.all(
                            color: alltaskdetailsList[index]
                                .taskProgressStatusColor,
                            width: 2)),
                    child: !isComplete
                        ? const Icon(Icons.check)
                        : Icon(Icons.circle,
                            color: alltaskdetailsList[index]
                                .taskProgressStatusColor,
                            size: 10),
                  ),
                ],
              ),
            ),
            Positioned(left: 100, bottom: 3, child: emptyTaskProgress()),
            Positioned(left: 190, bottom: 3, child: emptyTaskProgress()),
            Positioned(left: 280, bottom: 3, child: emptyTaskProgress()),
            Positioned(left: 360, bottom: 3, child: emptyTaskProgress()),
          ],
        ));
  }
}

Widget emptyTaskProgress() {
  return Row(
    children: [
      Container(
        width: 25,
        height: 25,
        decoration: const BoxDecoration(
          shape: BoxShape.circle,
          color: Color.fromARGB(255, 184, 184, 183),
        ),
        child: !isComplete
            ? const Icon(Icons.check)
            : const Icon(Icons.circle, color: Colors.white, size: 24),
      ),
    ],
  );
}

Widget displayAllTask() {
  return ListView.builder(
      itemCount: alltaskdetailsList.length,
      itemBuilder: (context, index) {
        return Padding(
          padding: const EdgeInsets.only(bottom: 10.0),
          child: Container(
            height: 210,
            decoration: const BoxDecoration(
                color: Color.fromARGB(255, 255, 255, 255),
                borderRadius: BorderRadius.all(Radius.circular(20)),
                boxShadow: [
                  BoxShadow(
                    color: Color.fromARGB(75, 158, 158, 158),
                    offset: Offset(0, 3),
                    blurRadius: 5,
                    spreadRadius: 3,
                  ),
                ]),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 10.0, top: 10),
                  child: Row(
                    children: [
                      Container(
                        height: 50,
                        width: 50,
                        decoration: BoxDecoration(
                            color: alltaskdetailsList[index].contImgColor,
                            borderRadius: BorderRadius.circular(10)),
                        child: const Icon(
                          Icons.add,
                          color: Colors.white,
                        ),
                      ),
                      const SizedBox(
                        width: 5,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              SizedBox(
                                width: 200,
                                child: Text(
                                  alltaskdetailsList[index].taskTitle,
                                  style: const TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18),
                                ),
                              ),
                              //const Spacer(),
                              const SizedBox(
                                width: 50,
                              ),
                              GestureDetector(
                                onTap: () {},
                                child: const Text(
                                  "View more",
                                  style: TextStyle(
                                    color: Color.fromARGB(255, 1, 158, 48),
                                    fontSize: 12,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Text(
                                alltaskdetailsList[index].taskDescription,
                                style: const TextStyle(
                                    fontWeight: FontWeight.normal,
                                    fontSize: 13),
                              ),
                              //const Spacer(),
                              const SizedBox(
                                width: 50,
                              ),
                              Container(
                                height: 25,
                                width: 55,
                                alignment: Alignment.center,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: alltaskdetailsList[index].taskRisk ==
                                          "High"
                                      ? const Color.fromARGB(33, 255, 86, 74)
                                      : const Color.fromARGB(40, 189, 238, 191),
                                ),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Icon(
                                      Icons.bar_chart_rounded,
                                      size: 15,
                                      color:
                                          alltaskdetailsList[index].taskRisk ==
                                                  "High"
                                              ? const Color.fromARGB(
                                                  255, 245, 61, 47)
                                              : const Color.fromARGB(
                                                  255, 68, 193, 72),
                                    ),
                                    Text(
                                      alltaskdetailsList[index].taskRisk,
                                      style: TextStyle(
                                        fontWeight: FontWeight.normal,
                                        fontSize: 12,
                                        color: alltaskdetailsList[index]
                                                    .taskRisk ==
                                                "High"
                                            ? const Color.fromARGB(
                                                255, 245, 61, 47)
                                            : const Color.fromARGB(
                                                255, 68, 193, 72),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              const SizedBox(
                                width: 20,
                              ),
                              Text(
                                alltaskdetailsList[index].taskWorker,
                                style: const TextStyle(
                                    fontWeight: FontWeight.normal,
                                    fontSize: 12),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                const Divider(
                  color: Color.fromARGB(110, 185, 185, 185),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10.0),
                  child: Row(
                    children: [
                      const Text(
                        "Progress Status:  ",
                        style: TextStyle(fontSize: 12),
                      ),
                      taskprogress(index),
                      const Spacer(),
                      Text(
                        alltaskdetailsList[index].taskDate,
                        style: const TextStyle(fontSize: 12),
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      Text(
                        alltaskdetailsList[index].taskTiming,
                        style: const TextStyle(fontSize: 12),
                      ),
                    ],
                  ),
                ),
                checkTaskDetails(index),
                Padding(
                  padding: const EdgeInsets.only(left: 12.0),
                  child: Row(
                    children: [
                      Text(checktaskProgress1(index),
                          style: TextStyle(
                            fontSize: 11,
                            fontWeight: FontWeight.normal,
                            color: checktaskProgressColor(index),
                          )),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20.0),
                  child: Row(
                    children: [
                      const Spacer(),
                      GestureDetector(
                        onTap: () {},
                        child: Container(
                          height: 35,
                          width: 90,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            border: Border.all(
                                width: 1, color: setBorderColor(index)),
                            color: setColor(index),
                            borderRadius: BorderRadius.circular(20),
                          ),
                          child: checktaskProgressDone(index),
                        ),
                      ),
                      const SizedBox(
                        width: 15,
                      ),
                      GestureDetector(
                        onTap: () {},
                        child: taskIsCompleted(index),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        );
      });
}

Widget completed() {
  return Row(
    children: [
      Container(
          width: 25,
          height: 25,
          decoration: const BoxDecoration(
            shape: BoxShape.circle,
            color: Colors.green,
          ),
          child: const Icon(
            Icons.check,
            size: 15,
            color: Colors.white,
          )),
    ],
  );
}

Widget checktaskProgressDone(int index) {
  if (alltaskdetailsList[index].taskCompleted) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
            height: 22,
            width: 22,
            decoration: BoxDecoration(
                border: Border.all(width: 1.5, color: Colors.white),
                shape: BoxShape.circle),
            child: const Icon(
              Icons.check,
              color: Color.fromARGB(255, 255, 255, 255),
              size: 18,
            )),
        const Text(
          "  Done",
          style: TextStyle(color: Color.fromARGB(255, 255, 255, 255)),
        )
      ],
    );
  } else if (alltaskdetailsList[index].taskStarted) {
    return const Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(Icons.play_circle_outline_rounded, color: Colors.green),
        Text(
          "  Start",
          style: TextStyle(color: Colors.green),
        )
      ],
    );
  } else {
    return const Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(Icons.check, color: Colors.green),
        Text(
          "  Done",
          style: TextStyle(color: Colors.green),
        )
      ],
    );
  }
}
