import 'package:flutter/material.dart';

class CreateCategoryContainer extends StatelessWidget {
  Color contColor;
  Color borderColor;
  Color circleColor;
  double height;
  double width;
  String taskCategory;
  int noTask;

  CreateCategoryContainer({
    required this.contColor,
    required this.borderColor,
    required this.circleColor,
    required this.height,
    required this.width,
    required this.taskCategory,
    required this.noTask,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width: width,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          color: contColor,
          border: Border.all(color: borderColor, width: 1)),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
                height: 20,
                width: 20,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: circleColor,
                ),
                child: const Icon(
                  Icons.circle,
                  size: 8,
                  color: Colors.white,
                )),
            const SizedBox(
              width: 15,
            ),
            Text(
              taskCategory,
              style:
                  const TextStyle(fontSize: 14, fontWeight: FontWeight.normal),
            ),
            const Spacer(),
            Text(
              "$noTask",
              style: const TextStyle(
                  color: Colors.black,
                  fontSize: 15,
                  fontWeight: FontWeight.bold),
            ),
          ],
        ),
      ),
    );
  }
}
