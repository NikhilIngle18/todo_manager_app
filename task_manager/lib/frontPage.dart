import 'package:flutter/material.dart';
import 'showBottomSheet.dart';

import 'view/displayCompleted.dart';
import 'view/displayInProgress.dart';
import 'view/displayToDo.dart';
import 'view/displayOverdue.dart';
import 'displayAllTask.dart';

import 'category.dart';

bool todotasksection = false;
bool inprogresstasksection = false;
bool overduetasksection = false;
bool completedtasksection = false;

List monthList = [
  "Jan 2024",
  "Feb 2024",
  "Mar 2024",
  "Apr 2024",
  "May 2024",
  "Jun 2024",
  "July 2024",
  "Aug 2024",
  "Sep 2024",
  "Oct 2024",
  "Nov 2024",
  "Dec 2024"
];
int i = 5;
List dateList = [07, 08, 09, 10, 11, 12, 13, 14];
List<String> dayList = [
  "Fri",
  "Sat",
  "Sun",
  "Mon",
  "Tue",
  "Wed",
  "Thu",
  "Fri",
];

class frontPage extends StatefulWidget {
  const frontPage({super.key});

  @override
  State<frontPage> createState() => _FrontPage();
}

class _FrontPage extends State<frontPage> {
  Widget displaySection() {
    if (todotasksection) {
      return displayToDo();
    } else if (inprogresstasksection) {
      return displayInProgress();
    } else if (overduetasksection) {
      return displayOverdue();
    } else if (completedtasksection) {
      return displayCompleted();
    } else {
      return displayAllTask();
    }
  }

  Widget dateSection(int index) {
    bool isClick = true;

    return Padding(
      padding: const EdgeInsets.only(left: 2.0),
      child: GestureDetector(
        onTap: () {
          setState(() {
            isClick = !isClick;
          });
        },
        child: Container(
          height: 20,
          width: 60,
          decoration: BoxDecoration(
            border: Border.all(
                width: 1.5,
                color: index == 0
                    ? const Color.fromARGB(221, 234, 54, 114)
                    : Colors.transparent),
            borderRadius: const BorderRadius.all(Radius.circular(15)),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "${dateList[index]}",
                style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                    color: index == 0
                        ? const Color.fromARGB(221, 234, 54, 114)
                        : const Color.fromARGB(230, 32, 32, 32)),
              ),
              const SizedBox(
                height: 10,
              ),
              Text(
                dayList[index],
                style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                    color: index == 0
                        ? const Color.fromARGB(221, 234, 54, 114)
                        : const Color.fromARGB(148, 48, 48, 48)),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 1.0),
        child: Column(
          children: [
            const SizedBox(
              height: 50,
            ),
            Row(
              children: [
                IconButton(
                  onPressed: () {},
                  icon: const Icon(
                    Icons.arrow_back,
                    size: 25,
                  ),
                  color: const Color.fromARGB(255, 1, 158, 48),
                ),
                const SizedBox(
                  width: 5,
                ),
                const Text(
                  "Manage Tasks",
                  style: TextStyle(
                      fontSize: 17,
                      fontWeight: FontWeight.bold,
                      color: Color.fromARGB(255, 1, 1, 1)),
                ),
                const SizedBox(
                  width: 5,
                ),
                const Icon(
                  Icons.info_outline,
                  size: 25,
                  color: Color.fromARGB(255, 1, 158, 48),
                ),
                const SizedBox(
                  width: 4,
                ),
                const Spacer(),
                GestureDetector(
                  onTap: () {},
                  child: Container(
                      height: 25,
                      width: 120,
                      decoration: const BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                          color: Color.fromARGB(255, 222, 15, 0)),
                      alignment: Alignment.center,
                      child: const Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.warning,
                            color: Colors.white,
                            size: 15,
                          ),
                          Text(
                            "  Incident Logs",
                            style: TextStyle(
                              fontSize: 12,
                              color: Color.fromRGBO(255, 255, 255, 1),
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ],
                      )),
                ),
                const SizedBox(
                  width: 5,
                ),
                IconButton(
                  onPressed: () {},
                  icon: const Icon(
                    Icons.search,
                    size: 30,
                  ),
                  color: const Color.fromARGB(255, 1, 158, 48),
                ),
              ],
            ),
            Row(
              children: [
                IconButton(
                    onPressed: () {
                      setState(() {
                        if (i > -1) {
                          i--;
                        } else {
                          i = 0;
                        }
                      });
                    },
                    icon: const Icon(
                      Icons.arrow_back_ios,
                      size: 20,
                      color: Color.fromARGB(255, 1, 158, 48),
                    )),
                Text(
                  monthList[i],
                  style: const TextStyle(
                      fontSize: 20, fontWeight: FontWeight.w500),
                ),
                IconButton(
                    onPressed: () {
                      setState(() {
                        if (i < monthList.length - 1) {
                          i++;
                        } else {
                          i = 0;
                        }
                      });
                    },
                    icon: const Icon(
                      Icons.arrow_forward_ios,
                      size: 20,
                      color: Color.fromARGB(255, 1, 158, 48),
                    )),
                const Spacer(),
                IconButton(
                    onPressed: () {},
                    icon: const Icon(
                      Icons.calendar_month_outlined,
                      size: 25,
                      color: Color.fromARGB(255, 1, 158, 48),
                    )),
              ],
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10.0),
              child: SizedBox(
                height: 80,
                child: ListView.builder(
                    itemCount: 8,
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (context, index) {
                      return dateSection(index);
                    }),
              ),
            ),
            const SizedBox(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                GestureDetector(
                  onTap: () {},
                  child: Container(
                    decoration: const BoxDecoration(
                        color: Color.fromARGB(74, 248, 209, 223),
                        shape: BoxShape.rectangle,
                        borderRadius: BorderRadius.all(Radius.circular(20))),
                    height: 40,
                    width: 160,
                    alignment: Alignment.center,
                    child: const Text(
                      "Task Overview",
                      style: TextStyle(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          color: Color.fromARGB(221, 234, 54, 114)),
                    ),
                  ),
                ),
                GestureDetector(
                  child: Container(
                    decoration: const BoxDecoration(
                        color: Colors.transparent,
                        shape: BoxShape.rectangle,
                        borderRadius: BorderRadius.all(Radius.circular(20))),
                    height: 40,
                    width: 160,
                    alignment: Alignment.center,
                    child: const Text(
                      "My SOP's",
                      style: TextStyle(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        color: Color.fromARGB(255, 0, 0, 0),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(height: 10),
            Padding(
                padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 5),
                child: SizedBox(
                  height: 90,
                  width: double.infinity,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Column(
                        children: [
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                todotasksection = !todotasksection;
                                overduetasksection = false;
                                completedtasksection = false;
                                inprogresstasksection = false;
                              });
                            },
                            child: CreateCategoryContainer(
                              width: 160,
                              height: 40,
                              taskCategory: "To Do Task",
                              noTask: 16,
                              circleColor:
                                  const Color.fromARGB(255, 237, 214, 3),
                              contColor: todotasksection
                                  ? const Color.fromARGB(209, 251, 248, 212)
                                  : const Color.fromARGB(209, 255, 254, 244),
                              borderColor: todotasksection
                                  ? const Color.fromARGB(255, 237, 214, 3)
                                  : const Color.fromARGB(209, 255, 254, 244),
                            ),
                          ),
                          const SizedBox(height: 10),
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                todotasksection = false;
                                overduetasksection = false;
                                completedtasksection = false;
                                inprogresstasksection = !inprogresstasksection;
                              });
                            },
                            child: CreateCategoryContainer(
                              width: 160,
                              height: 40,
                              taskCategory: "In-Progress",
                              noTask: 16,
                              circleColor:
                                  const Color.fromARGB(255, 237, 163, 3),
                              contColor: inprogresstasksection
                                  ? const Color.fromARGB(120, 251, 239, 212)
                                  : const Color.fromARGB(120, 254, 245, 240),
                              borderColor: inprogresstasksection
                                  ? const Color.fromARGB(255, 237, 163, 3)
                                  : const Color.fromARGB(120, 254, 245, 240),
                            ),
                          )
                        ],
                      ),
                      Column(
                        children: [
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                todotasksection = false;
                                overduetasksection = !overduetasksection;
                                completedtasksection = false;
                                inprogresstasksection = false;
                              });
                            },
                            child: CreateCategoryContainer(
                              taskCategory: "Overdue",
                              width: 160,
                              height: 40,
                              noTask: 16,
                              circleColor:
                                  const Color.fromARGB(255, 237, 85, 3),
                              contColor: overduetasksection
                                  ? const Color.fromARGB(120, 251, 220, 212)
                                  : const Color.fromARGB(120, 255, 241, 238),
                              borderColor: overduetasksection
                                  ? const Color.fromARGB(255, 237, 85, 3)
                                  : const Color.fromARGB(120, 255, 241, 238),
                            ),
                          ),
                          const SizedBox(height: 10),
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                todotasksection = false;
                                overduetasksection = false;
                                completedtasksection = !completedtasksection;
                                inprogresstasksection = false;
                              });
                            },
                            child: CreateCategoryContainer(
                              taskCategory: "Completed",
                              height: 40,
                              width: 160,
                              noTask: 16,
                              circleColor:
                                  const Color.fromARGB(255, 24, 128, 1),
                              contColor: completedtasksection
                                  ? const Color.fromARGB(120, 227, 251, 217)
                                  : const Color.fromARGB(120, 238, 251, 232),
                              borderColor: completedtasksection
                                  ? const Color.fromARGB(255, 24, 128, 1)
                                  : const Color.fromARGB(120, 238, 251, 232),
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                )),
            SizedBox(
              width: double.infinity,
              height: 410,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 5),
                child: displaySection(),
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: Padding(
        padding: const EdgeInsets.only(bottom: 160.0),
        child: FloatingActionButton(
          onPressed: bottomSheet,
          child: Container(
            width: 56,
            height: 56,
            decoration: const BoxDecoration(
                color: Colors.pink,
                borderRadius: BorderRadius.all(Radius.circular(10))),
            child: const Icon(
              Icons.add,
              color: Colors.white,
              size: 30,
            ),
          ),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
    );
  }

  Future bottomSheet() {
    return showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      useRootNavigator: false,
      useSafeArea: true,
      builder: (context) => const SafeArea(
        child: TaskDetailsClass(),
      ),
    );
  }
}
