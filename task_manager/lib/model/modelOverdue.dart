import "package:flutter/material.dart";

List<OverDueTaskDetails> overduetasksList = [
  OverDueTaskDetails(
    contImgColor: const Color.fromARGB(255, 95, 204, 234),
    taskTitle: "Room 303 Set up",
    taskDescription: "Housekeeping",
    taskDate: "14 July 2024",
    taskRisk: "High",
    taskTiming: "5:00 pm",
    taskWorker: "Garima Bhatia",
    taskOverdue: true,
    taskCompleted: true,
  ),
  OverDueTaskDetails(
    contImgColor: const Color.fromARGB(255, 184, 147, 236),
    taskTitle: "Fire Place check up",
    taskDescription: "Maintainence &..",
    taskDate: "16 July 2024",
    taskRisk: "Low",
    taskTiming: "4:00 pm",
    taskWorker: "Raghu Kumar",
    taskOverdue: true,
    taskCompleted: false,
  ),
  OverDueTaskDetails(
    contImgColor: const Color.fromARGB(255, 184, 147, 236),
    taskTitle: "Water leakage repair",
    taskDescription: "raise an incedent",
    taskDate: "14 July 2024",
    taskRisk: "Low",
    taskTiming: "5:00 pm",
    taskWorker: "mahesh",
    taskOverdue: true,
    taskCompleted: false,
  ),
  OverDueTaskDetails(
    contImgColor: const Color.fromARGB(255, 184, 147, 236),
    taskTitle: "Office Area",
    taskDescription: "Maintainence &..",
    taskDate: "20 July 2024",
    taskRisk: "High",
    taskTiming: "12:00 pm",
    taskWorker: "Anil Gupta",
    taskOverdue: true,
    taskCompleted: true,
  ),
];

class OverDueTaskDetails {
  Color contImgColor;
  String taskTitle;
  String taskDescription;
  String taskDate;
  String taskRisk;
  String taskTiming;
  String taskWorker;
  bool taskOverdue;
  bool taskCompleted;

  OverDueTaskDetails({
    required this.contImgColor,
    required this.taskTitle,
    required this.taskOverdue,
    required this.taskDescription,
    required this.taskDate,
    required this.taskRisk,
    required this.taskTiming,
    required this.taskWorker,
    required this.taskCompleted,
  });
}
