import 'package:flutter/material.dart';

class TaskDetailView extends StatelessWidget {
  double height = 210;
  Color contImgColor;
  String taskTitle;
  String taskDescription;
  String taskWorker;
  double taskProgressStatus;
  String taskDate;
  String taskTiming;
  String taskRisk;
  Color taskProgressStatusColor;
  bool taskCompleted;
  bool taskInfo;
  bool taskStarted;
  bool taskOverdue;

  TaskDetailView(
      {super.key,
      required this.taskInfo,
      required this.taskStarted,
      required this.taskCompleted,
      required this.taskOverdue,
      required this.contImgColor,
      required this.taskDate,
      required this.taskTiming,
      required this.taskRisk,
      required this.taskTitle,
      required this.taskWorker,
      required this.taskDescription,
      required this.taskProgressStatus,
      required this.taskProgressStatusColor});
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}

List<TaskDetailView> alltaskdetailsList = [
  TaskDetailView(
    contImgColor: const Color.fromARGB(255, 95, 204, 234),
    taskTitle: "Room 303 Set up",
    taskDescription: "Housekeeping",
    taskCompleted: false,
    taskInfo: true,
    taskOverdue: false,
    taskStarted: true,
    taskDate: "14 July 2024",
    taskProgressStatus: 30,
    taskRisk: "High",
    taskTiming: "5:00 pm",
    taskWorker: "Garima Bhatia",
    taskProgressStatusColor: Colors.amber,
  ),
  TaskDetailView(
    taskCompleted: true,
    taskInfo: false,
    taskOverdue: false,
    taskStarted: false,
    contImgColor: const Color.fromARGB(255, 184, 147, 236),
    taskTitle: "Fire Place check up",
    taskDescription: "Maintainence &..",
    taskDate: "16 July 2024",
    taskProgressStatus: 30,
    taskRisk: "Low",
    taskTiming: "4:00 pm",
    taskWorker: "Raghu Bhatia",
    taskProgressStatusColor: const Color.fromARGB(255, 214, 110, 222),
  ),
  TaskDetailView(
    contImgColor: const Color.fromARGB(255, 95, 204, 234),
    taskTitle: "Water leakage repair",
    taskDescription: "raise an incedent",
    taskCompleted: false,
    taskInfo: false,
    taskOverdue: false,
    taskStarted: true,
    taskDate: "15 July 2024",
    taskProgressStatus: 40,
    taskRisk: "Low",
    taskTiming: "5:00 pm",
    taskWorker: "mahesh",
    taskProgressStatusColor: Colors.yellow,
  ),
  TaskDetailView(
    contImgColor: const Color.fromARGB(255, 95, 204, 234),
    taskTitle: "shopping",
    taskDescription: "Dresses",
    taskCompleted: false,
    taskInfo: false,
    taskOverdue: false,
    taskStarted: true,
    taskDate: "14 July 2024",
    taskProgressStatus: 40,
    taskRisk: "Low",
    taskTiming: "5:00 pm",
    taskWorker: "Garima Bhatia",
    taskProgressStatusColor: const Color.fromARGB(255, 208, 146, 23),
  ),
  TaskDetailView(
    contImgColor: const Color.fromARGB(255, 95, 204, 234),
    taskTitle: "Door lock installation",
    taskDescription: "coustom",
    taskCompleted: false,
    taskInfo: false,
    taskOverdue: false,
    taskStarted: false,
    taskDate: "14 July 2024",
    taskProgressStatus: 0,
    taskRisk: "High",
    taskTiming: "5:00 pm",
    taskWorker: "raghunath",
    taskProgressStatusColor: const Color.fromARGB(255, 208, 146, 23),
  ),
];
