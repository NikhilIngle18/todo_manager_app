import '../model/modelInProgress.dart';
import 'package:flutter/material.dart';

Widget displayInProgress() {
  return ListView.builder(
    itemCount: inprogresstasksList.length,
    itemBuilder: (context, index) {
      return Padding(
        padding: const EdgeInsets.only(bottom: 10.0),
        child: Container(
          height: 210,
          decoration: const BoxDecoration(
              color: Color.fromARGB(255, 255, 255, 255),
              borderRadius: BorderRadius.all(Radius.circular(20)),
              boxShadow: [
                BoxShadow(
                  color: Color.fromARGB(75, 158, 158, 158),
                  offset: Offset(0, 3),
                  blurRadius: 5,
                  spreadRadius: 3,
                ),
              ]),
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 9, top: 10),
                child: Row(
                  children: [
                    Container(
                      height: 50,
                      width: 50,
                      decoration: BoxDecoration(
                          color: inprogresstasksList[index].contImgColor,
                          borderRadius: BorderRadius.circular(10)),
                      child: const Icon(
                        Icons.add,
                        color: Colors.white,
                      ),
                    ),
                    const SizedBox(
                      width: 5,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            SizedBox(
                              width: 200,
                              child: Text(
                                inprogresstasksList[index].taskTitle,
                                style: const TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 18),
                              ),
                            ),
                            //const Spacer(),
                            const SizedBox(
                              width: 50,
                            ),
                            GestureDetector(
                              onTap: () {},
                              child: const Text(
                                "View more",
                                style: TextStyle(
                                  color: Color.fromARGB(255, 1, 158, 48),
                                  fontSize: 12,
                                ),
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Text(
                              inprogresstasksList[index].taskDescription,
                              style: const TextStyle(
                                  fontWeight: FontWeight.normal, fontSize: 13),
                            ),
                            //const Spacer(),
                            const SizedBox(
                              width: 50,
                            ),
                            Container(
                              height: 25,
                              width: 55,
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: inprogresstasksList[index].taskRisk ==
                                        "High"
                                    ? const Color.fromARGB(33, 255, 86, 74)
                                    : const Color.fromARGB(40, 189, 238, 191),
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Icon(
                                    Icons.bar_chart_rounded,
                                    size: 15,
                                    color: inprogresstasksList[index]
                                                .taskRisk ==
                                            "High"
                                        ? const Color.fromARGB(255, 245, 61, 47)
                                        : const Color.fromARGB(
                                            255, 68, 193, 72),
                                  ),
                                  Text(
                                    inprogresstasksList[index].taskRisk,
                                    style: TextStyle(
                                      fontWeight: FontWeight.normal,
                                      fontSize: 12,
                                      color: inprogresstasksList[index]
                                                  .taskRisk ==
                                              "High"
                                          ? const Color.fromARGB(
                                              255, 245, 61, 47)
                                          : const Color.fromARGB(
                                              255, 68, 193, 72),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            const SizedBox(
                              width: 20,
                            ),
                            Text(
                              inprogresstasksList[index].taskWorker,
                              style: const TextStyle(
                                  fontWeight: FontWeight.normal, fontSize: 12),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const Divider(
                color: Color.fromARGB(110, 185, 185, 185),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10.0),
                child: Row(
                  children: [
                    const Text(
                      "Progress Status:  ",
                      style: TextStyle(fontSize: 12),
                    ),
                    Text(
                      // ignore: prefer_adjacent_string_concatenation
                      "${inprogresstasksList[index].taskProgressStatus}" + "%",
                      style: TextStyle(
                          color: inprogresstasksList[index]
                              .taskProgressStatusColor),
                    ),
                    const Spacer(),
                    Text(
                      inprogresstasksList[index].taskDate,
                      style: const TextStyle(fontSize: 12),
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    Text(
                      inprogresstasksList[index].taskTiming,
                      style: const TextStyle(fontSize: 12),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10.0),
                child: Stack(
                  children: [
                    SizedBox(
                      height: 30,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Stack(children: [
                            Container(
                              height: 2,
                              width: 360,
                              color: Colors.grey,
                            ),
                            Positioned(
                              child: Container(
                                height: 2,
                                width: 190,
                                color: inprogresstasksList[index]
                                    .taskProgressStatusColor,
                              ),
                            ),
                          ])
                        ],
                      ),
                    ),
                    Positioned(
                      left: 10,
                      bottom: 3,
                      child: Row(
                        children: [
                          Container(
                            width: 24,
                            height: 25,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: inprogresstasksList[index]
                                  .taskProgressStatusColor,
                            ),
                            child: isComplete
                                ? const Icon(
                                    Icons.check,
                                    color: Colors.white,
                                    size: 15,
                                  )
                                : const Icon(Icons.circle, color: Colors.white),
                          ),
                        ],
                      ),
                    ),
                    Positioned(
                      left: 100,
                      bottom: 3,
                      child: Row(
                        children: [
                          Container(
                            width: 25,
                            height: 25,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: inprogresstasksList[index]
                                  .taskProgressStatusColor,
                            ),
                            child: isComplete
                                ? const Icon(
                                    Icons.check,
                                    color: Colors.white,
                                    size: 15,
                                  )
                                : const Icon(Icons.circle, color: Colors.white),
                          ),
                        ],
                      ),
                    ),
                    Positioned(
                      left: 190,
                      bottom: 3,
                      child: Row(
                        children: [
                          Container(
                            width: 25,
                            height: 25,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Colors.white,
                                border: Border.all(
                                    color: inprogresstasksList[index]
                                        .taskProgressStatusColor,
                                    width: 2)),
                            child: !isComplete
                                ? const Icon(Icons.check)
                                : Icon(Icons.circle,
                                    color: inprogresstasksList[index]
                                        .taskProgressStatusColor,
                                    size: 10),
                          ),
                        ],
                      ),
                    ),
                    Positioned(
                      left: 280,
                      bottom: 3,
                      child: Row(
                        children: [
                          Container(
                            width: 25,
                            height: 25,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Colors.white,
                                border:
                                    Border.all(color: Colors.grey, width: 2)),
                            child: !isComplete
                                ? const Icon(Icons.check)
                                : const Icon(Icons.circle,
                                    color: Color.fromARGB(170, 120, 120, 120),
                                    size: 10),
                          ),
                        ],
                      ),
                    ),
                    Positioned(
                      left: 360,
                      bottom: 3,
                      child: Row(
                        children: [
                          Container(
                            width: 25,
                            height: 25,
                            decoration: const BoxDecoration(
                              shape: BoxShape.circle,
                              color: Color.fromARGB(255, 184, 184, 183),
                            ),
                            child: !isComplete
                                ? const Icon(Icons.check)
                                : const Icon(Icons.circle,
                                    color: Colors.white, size: 24),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 12.0),
                child: Row(
                  children: [
                    Text("In Progress",
                        style: TextStyle(
                            fontSize: 11,
                            fontWeight: FontWeight.normal,
                            color: inprogresstasksList[index]
                                .taskProgressStatusColor)),
                  ],
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 20.0, vertical: 5),
                child: Row(
                  children: [
                    const Spacer(),
                    GestureDetector(
                      onTap: () {},
                      child: Container(
                        height: 35,
                        width: 90,
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          border: Border.all(width: 1, color: setColor(index)),
                          color: Colors.transparent,
                          borderRadius: BorderRadius.circular(20),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                                height: 22,
                                width: 22,
                                decoration: BoxDecoration(
                                    border: Border.all(
                                        width: 1.5, color: setColor(index)),
                                    shape: BoxShape.circle),
                                child: Icon(
                                  Icons.play_arrow,
                                  color: setColor(index),
                                  size: 18,
                                )),
                            Text(
                              "  Start",
                              style: TextStyle(color: setColor(index)),
                            )
                          ],
                        ),
                      ),
                    ),
                    const SizedBox(
                      width: 15,
                    ),
                    GestureDetector(
                      onTap: () {},
                      child: Container(
                          height: 25,
                          width: 25,
                          decoration: BoxDecoration(
                              border: Border.all(
                                  width: 1.5, color: setColor(index)),
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(8)),
                              shape: BoxShape.rectangle),
                          child: Icon(
                            Icons.more_horiz_rounded,
                            color: setColor(index),
                            size: 18,
                          )),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      );
    },
  );
}

Color setColor(int index) {
  return Colors.green;
}

Color setBorderColor(int index) {
  return Colors.green;
}

Widget taskIsCompleted(int index) {
  return Container(
      height: 25,
      width: 25,
      decoration: BoxDecoration(
          border: Border.all(width: 1.5, color: Colors.green),
          borderRadius: const BorderRadius.all(Radius.circular(8)),
          shape: BoxShape.rectangle),
      child: const Icon(
        Icons.more_horiz_rounded,
        color: Colors.green,
        size: 18,
      ));
}

Widget checktaskProgressDone(int index) {
  if (inprogresstasksList[index].taskStarted) {
    return const Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(Icons.play_circle_outline_rounded, color: Colors.green),
        Text(
          "  Start",
          style: TextStyle(color: Colors.green),
        )
      ],
    );
  } else {
    return const Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(Icons.check, color: Colors.green),
        Text(
          "  Done",
          style: TextStyle(color: Colors.green),
        )
      ],
    );
  }
}

Widget taskprogress(int index) {
  return Text(
    "${inprogresstasksList[index].taskProgressStatus}",
    style: TextStyle(color: inprogresstasksList[index].taskProgressStatusColor),
  );
}

Color? checktaskProgressColor(int index) {
  return inprogresstasksList[index].taskProgressStatusColor;
}

String checktaskProgress1() {
  return "In Progress";
}

Widget checkTaskDetails(int index) {
  return Padding(
    padding: const EdgeInsets.only(top: 20.0),
    child: Stack(
      children: [
        SizedBox(
          height: 30,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Stack(children: [
                Container(
                  height: 2,
                  width: 360,
                  color: Colors.grey,
                ),
                Positioned(
                  child: Container(
                    height: 2,
                    width: 150,
                    color: inprogresstasksList[index].taskProgressStatusColor,
                  ),
                ),
              ])
            ],
          ),
        ),
        Positioned(
          left: 10,
          bottom: 3,
          child: Row(
            children: [
              Container(
                width: 25,
                height: 25,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: inprogresstasksList[index].taskProgressStatusColor,
                ),
                child: isComplete
                    ? const Icon(
                        Icons.check,
                        color: Colors.white,
                        size: 15,
                      )
                    : const Icon(Icons.circle, color: Colors.white),
              ),
            ],
          ),
        ),
        Positioned(
          left: 100,
          bottom: 3,
          child: Row(
            children: [
              Container(
                width: 25,
                height: 25,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: inprogresstasksList[index].taskProgressStatusColor,
                ),
                child: isComplete
                    ? const Icon(
                        Icons.check,
                        color: Colors.white,
                        size: 15,
                      )
                    : const Icon(Icons.circle, color: Colors.white),
              ),
            ],
          ),
        ),
        Positioned(
          left: 190,
          bottom: 3,
          child: Row(
            children: [
              Container(
                width: 25,
                height: 25,
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.white,
                    border: Border.all(
                        color:
                            inprogresstasksList[index].taskProgressStatusColor,
                        width: 2)),
                child: !isComplete
                    ? const Icon(Icons.check)
                    : Icon(Icons.circle,
                        color:
                            inprogresstasksList[index].taskProgressStatusColor,
                        size: 10),
              ),
            ],
          ),
        ),
        Positioned(
          left: 280,
          bottom: 3,
          child: Row(
            children: [
              Container(
                width: 25,
                height: 25,
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.white,
                    border: Border.all(color: Colors.grey, width: 2)),
                child: !isComplete
                    ? const Icon(Icons.check)
                    : const Icon(Icons.circle,
                        color: Color.fromARGB(170, 120, 120, 120), size: 10),
              ),
            ],
          ),
        ),
        Positioned(
          left: 360,
          bottom: 3,
          child: Row(
            children: [
              Container(
                width: 25,
                height: 25,
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                  color: Color.fromARGB(255, 184, 184, 183),
                ),
                child: !isComplete
                    ? const Icon(Icons.check)
                    : const Icon(Icons.circle, color: Colors.white, size: 24),
              ),
            ],
          ),
        )
      ],
    ),
  );
}

bool isComplete = true;

Widget emptyTaskProgress() {
  return Row(
    children: [
      Container(
        width: 25,
        height: 25,
        decoration: const BoxDecoration(
          shape: BoxShape.circle,
          color: Color.fromARGB(255, 184, 184, 183),
        ),
        child: !isComplete
            ? const Icon(Icons.check)
            : const Icon(Icons.circle, color: Colors.white, size: 24),
      ),
    ],
  );
}

Widget completed() {
  return Row(
    children: [
      Container(
          width: 25,
          height: 25,
          decoration: const BoxDecoration(
            shape: BoxShape.circle,
            color: Colors.green,
          ),
          child: const Icon(
            Icons.check,
            size: 15,
            color: Colors.white,
          )),
    ],
  );
}
